FROM debian:10.2
MAINTAINER informatique@agepoly.ch
ARG version=0.2
ARG project_name="petitesannonces"

RUN echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections

RUN apt-get update
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8

ENV DEBIAN_FRONTEND noninteractive
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US
ENV LC_ALL en_US.UTF-8

RUN apt-get update
RUN apt-get install -y \
    apache2 \
    git \
    libapache2-mod-wsgi \
    memcached \
    python-pip \
    python-memcache \
    python-mysqldb \
    python-psycopg2 \
    python-yaml \
    python-typing \
    sqlite \
    postfix \
    s6


RUN a2dissite 000-default
COPY ./Deployement/files/apache.conf /etc/apache2/sites-available/$project_name.conf
RUN a2ensite $project_name


# Copy django project from agepoly gitlab

ARG git_branch="master"
ARG git_repo_path="agepoly/it/dev/petitesannonces"
ARG git_temp_path="/tmp/petitesannonces"
ARG pip_reqs=""
ARG www_root_path="/var/www/git-repo"

RUN git clone  --progress --verbose --single-branch  --branch $git_branch https://gitlab.com/$git_repo_path $git_temp_path

WORKDIR $git_temp_path

RUN mkdir $www_root_path && cp -r $git_temp_path/ -t $www_root_path

RUN find  "$www_root_path"
RUN ls -l "$git_temp_path"

WORKDIR "$www_root_path"

RUN rm -fr $git_temp_path

RUN pip install -r $www_root_path/petitesannonces/petitesannonces/data/pip-reqs.txt

# Downgrade the django-simple-captcha version 

RUN pip uninstall -y django-simple-captcha

COPY ./extra/django-simple-captcha-0.4.4.tar.gz \
     $www_root_path/petitesannonces/django-simple-captcha-0.4.4.tar.gz
RUN tar xzf $www_root_path/petitesannonces/django-simple-captcha-0.4.4.tar.gz -C /tmp
RUN pip install django-bootstrap3==5.1.1
RUN mv /tmp/django-simple-captcha-0.4.4/captcha /usr/local/lib/python2.7/dist-packages/captcha

RUN apt-get purge -y git
RUN apt-get clean

# Copy django config, synchronise static files and database model

#COPY --chown=www-data:www-data ./Deployement/files/settingsLocal.py \
#     $www_root_path/petitesannonces/petitesannonces/app/settingsLocal.py

# chown www-data:www-data  "/var/www/git-repo/petitesannonces/petitesannonces/app/settingsLocal.py" 
RUN chown -R www-data:www-data /var/www/

# For debuging local docker
#COPY --chown=www-data:www-data ./petitesannonces/app/config.yml /var/www/git-repo/petitesannonces/petitesannonces/app/config.yml

RUN su www-data -s /bin/bash -c "/var/www/git-repo/petitesannonces/petitesannonces/manage.py collectstatic --noinput"

COPY ./s6 /lib/s6

# Service and port exposure

EXPOSE 80

CMD "/lib/s6/init"
