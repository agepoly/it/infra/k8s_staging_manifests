---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: studyingtips
  labels:
    name: studyingtips
    owner: agepprev
spec:
  replicas: 1
  selector:
    matchLabels:
      app: studyingtips
      owner: agepprev
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: studyingtips
        owner: agepprev
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      initContainers:
        - name: init-container
          image: gitlab.com/agepoly/dependency_proxy/containers/busybox
          command: ['sh', '-c', 'chown -R 33:33 /var/www/html/wp-content/']
          volumeMounts:
            - {"name":"studyingtips-wordpress-content-v","mountPath":"/var/www/html/wp-content/"}
      containers:
        - name: studyingtips
          image: registry.gitlab.com/agepoly/it/infra/kubernetes/studyingtips:665b6dd2
          ports:
            - containerPort: 80
          env:
            - name: WORDPRESS_DB_HOST
              valueFrom:
                secretKeyRef:
                  name: studyingtips-env-secrets
                  key: WORDPRESS_DB_HOST
            - name: WORDPRESS_DB_USER
              valueFrom:
                secretKeyRef:
                  name: studyingtips-env-secrets
                  key: WORDPRESS_DB_USER
            - name: WORDPRESS_DB_NAME
              valueFrom:
                secretKeyRef:
                  name: studyingtips-env-secrets
                  key: WORDPRESS_DB_NAME
            - name: WORDPRESS_DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: studyingtips-env-secrets
                  key: WORDPRESS_DB_PASSWORD
            - name: WORDPRESS_CONFIG_EXTRA
              value: "define(\"WP_SITEURL\", \"https://\".explode(\":\", getenv(\"HOST\"))[0].\"/\");\ndefine(\"WP_HOME\",   \"https://\".explode(\":\", getenv(\"HOST\"))[0].\"/\");\n"
            - name: HOST
              value: "studyingtips.staging.agepoly.ch"
          volumeMounts:
            - {"name":"studyingtips-wordpress-content-v","mountPath":"/var/www/html/wp-content/"}
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: studyingtips.staging.agepoly.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: studyingtips.staging.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: studyingtips.staging.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10
      nodeSelector:
        storage_node: "true"
      volumes:
        - {"persistentVolumeClaim":{"claimName":"studyingtips-wordpress-content-pvc"},"name":"studyingtips-wordpress-content-v"}

---
apiVersion: v1
kind: Service
metadata:
  name: studyingtips
  labels:
    name: studyingtips
    owner: agepprev
spec:
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
  selector:
      app: studyingtips
      owner: agepprev
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: studyingtips-main-domain
  labels:
    name: studyingtips
    owner: agepprev
spec:
  entryPoints:
    - websecure
  tls:
    secretName: staging.agepoly.ch-wildcard-ssl
  routes:
    - match: Host(`studyingtips.staging.agepoly.ch`)
      kind: Rule
      services:
        - name: studyingtips
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: studyingtips-wordpress-content-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 3G
